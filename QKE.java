import java.util.*;
import java.io.*;

class QKE{

    //Initilize vaiables.
    private static ArrayList<Qubit> transmitterList = new ArrayList<Qubit>();
    private static ArrayList<Qubit> receiverList = new ArrayList<Qubit>();
    private static ArrayList<Qubit> measuredList = new ArrayList<Qubit>();
    private static Random rand = new Random();
    private static int streamLength = 16;
    
    public static void main(String[] args){
        try{
            transmitter();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    //Creates qubit stream to be tranmitted.
    public static void transmitter(){
        try{
        //For as the stream length, create qubits from random values
        //and polarizations.
        for(int i = 0; i < streamLength; i++){
            int num = rand.nextInt(10);
            int num2 = rand.nextInt(10);
            int value = 0;
            int polarization = 0;

            //Calls randomizers.
            value = randomValue(num);
            polarization = randomPolarization(num2);
            //Contructs new qubit based on returned value and polarization.
            Qubit q = new Qubit(value, polarization);
            transmitterList.add(q);
        }
        //Calls receiver method and passes in list created.
        receiver(transmitterList);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    //Receives list from transmitter, makes random polarizations and measures
    //against values from transmitter list. 
    public static void receiver(ArrayList<Qubit> list){
        try{

            for(Qubit k : list){
                receiverList.add(k);
            }
        //For each qubit in receiver list measure against value from list.
        //Create new qubit to be save polarization and values gained from measureing.
        for(Qubit q : receiverList){
            int num = rand.nextInt(10);
            int polarization = 0;
            int measureValue = 0;

            polarization = randomPolarization(num);

            int mValue = q.measure(polarization);
            Qubit newQ = new Qubit(mValue, polarization);
            measuredList.add(newQ);
        }
        polarizationExchange(receiverList, measuredList);

        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    //Compares polarizations from transmit qubits and measured qubits to
    //find matches in polarizations.
    public static void polarizationExchange(ArrayList<Qubit> transmit, ArrayList<Qubit> measure){
        ArrayList<Integer> posMatch = new ArrayList<Integer>();
        //For each value in transmit size, get the polarization value.
        for(int i =0; i < transmit.size(); i++){

            int transmitPol = transmit.get(i).getPolarization();
            int measurePol = measure.get(i).getPolarization();
            //If the values from each qubit polarization are equal then add  
            //the position of the match to match list.
            if(transmitPol == measurePol){
                posMatch.add(i);
            }
        }
        //Call get key.
        getKey(posMatch, transmit, measure);
    }

    //Gets the shared key by using the match list created and the existing
    //qubit stream lists.
    public static void getKey(ArrayList<Integer> match, ArrayList<Qubit> transmit, ArrayList<Qubit> receiver){

        String transmitKey = "";
        String receiverKey = "";

        //For each element in match list get the postion of the match
        //and add the value at that corresponding match to the shared key string.
        for(int i = 0; i < match.size(); i++){
            int num = match.get(i);

            transmitKey += Integer.toString(transmit.get(num).getValue());
            receiverKey += Integer.toString(receiver.get(num).getValue());
        }
        System.out.println(transmitKey + "\n" + receiverKey);
        //If secret key matches call encypt message.
        if(transmitKey.equals(receiverKey)){
            System.out.println("Same secret key!");
            encyptMessage(transmitKey);
        }
        else{
            System.out.println("Not the same secret key.");
        }


    }

    //Uses XOR class to encypt and decrypt message using shared secret key found.
    public static void encyptMessage(String key){
        XOR x = new XOR();
        String message = "any";

        String encryption = x.encrypt(message, key);
        String decryption = x.decrypt(encryption, key);

        System.out.println("\n" + "BEFORE ENCRYPT: " + message + "\n" + "AFTER ENCRYPT: " + encryption + "\n" + "MESSAGE DECYPTED: " + decryption);
    }

    //Returns either 0 or 1 randomly.
    public static int randomValue(int rand){
        int value = 0;

        if(rand > 5){
            value = 1;
        }
        else{
            value = 0;
        }
        return value;
    }
    //Returns either 0 or 1 randomly.
    public static int randomPolarization(int rand){
        int polarization = 0;

        if(rand > 5){
            polarization = 1;
        }
        else{
            polarization = 0;
        }
        return polarization;
    }
}