import java.util.*;
import java.io.*;

class XOR{

    public XOR(){
    }

    //Encrypt data string by conveting to binary and XOR'ing with shared key.
    public static String encrypt(String data, String key){
        
        if(key != null && data != null){
        int counter = 0;
        //Convert message to binary
        String bData = dataToBinary(data);
        String encryption = "";

        //For each value in our converted message, XOR with corresponding key value.
        //what ever value is returned by the XOR it is added to the encryption string.
        for(int i = 0; i < bData.length(); i++){
            //If value is not a space continue.
            if(bData.charAt(i) != ' '){
                //If counter is less than the keys length, XOR'ing can take place.
                if(counter < key.length()){
                    char c = key.charAt(counter);
                    
                    int compare = bData.charAt(i) ^ c;
                    encryption += Integer.toString(compare);
                    counter ++;
                }
                //Else set counter to zero so XOR'ing can continue.
                else{
                    counter = 0;
                    i --;
                }
            }
            //Add space to encryption.
            else{
                encryption += ' ';
            }
        }
        return encryption;
        }
        else{
            return "INVALID DATA OR KEY.";
        }
    }

    //Decrypt binary number input with shared key.
    public static String decrypt(String data, String key){

        if(key != null && data != null){
        int counter = 0;
        String decryption = "";
        //For each value in encypted message, XOR with secret key to return back to
        //Unencrypted state.
        for(int i = 0; i < data.length(); i++){
            //If value is not a space continue.
            if(data.charAt(i) != ' '){
                //If counter is less than the keys length, XOR'ing can take place.
                if(counter < key.length()){
                    char c = key.charAt(counter);
                
                    int compare = data.charAt(i) ^ c;
                    decryption += Integer.toString(compare);
                    counter ++;
                }
                //Else set counter to zero so XOR'ing can continue.
                else{
                    counter = 0;
                    i --;
                }
            }
            //Add space to decryption.
            else{
                decryption += ' ';
            }
        }
        
        //Turn binary decryption back to string.
        String result = binaryToString(decryption);
        return result;
        }
        else{
            return "INVALID KEY OR DATA.";
        }


    }
    
    //Turns binary input back to text.
    public static String binaryToString(String data){
        String[] parts = data.split(" ");
        StringBuilder sb = new StringBuilder();

        for(String part: parts){
            int val = Integer.parseInt(part, 2);
            String c = Character.toString(val);
            sb.append(c);
        }
        return sb.toString();

    }
    
    //Converts data received into binary output.
    public static String dataToBinary(String data){
        byte[] bytes = data.getBytes();

        StringBuilder sb = new StringBuilder();

        for(byte b: bytes){
            getBits(sb, b);

        }
        return sb.toString();
    }
    public static void getBits(StringBuilder sb, byte b){
        for(int i = 0; i < 8; i++){
            sb.append((b & 128) == 0 ? 0 : 1);
            b <<= 1;
        }
        sb.append(' ');
    }

}