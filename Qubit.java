import java.util.*;

public class Qubit{

    //Initailizes variables.
    private int value;
    //0 for circular, 1 for linear.
    private int polarization;
    private Random rand = new Random();

    //Qubit contructor.
    public Qubit(int value, int polarization){
        try{
            //Only allow values that are either 1 or 0.
            if((value == 0 || value == 1) && (polarization == 0 || polarization == 1)){
                this.value = value;
                this.polarization = polarization;
            }
            else{
                throw new Exception("Values must be 0 or 1");
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
    //Get qubit value and polarization.
    public String getQubit(){
        return this.value + ", " + this.polarization;
    }
    //Get qubit value.
    public int getValue(){
        return this.value;
    }
    //Get qubit polarization.
    public int getPolarization(){
        return this.polarization;
    }
    //Sets value and polarization of qubit.
    public void set(int value, int polarization){
        if((value == 0 || value == 1) && (polarization == 0 || polarization == 1)){
            this.value = value;
            this.polarization = polarization;
        }
    }

    //Compares input polarization with acctual polarization.
    public int measure(int polarization){
        int newValue = this.value;

        if(polarization == 0 || polarization == 1){
            //If input is equall to actual polarization then return current value.
            if(polarization == this.polarization){
                return newValue;
            }
            //Else return random value, either 1 or 0.
            else{
                int number = rand.nextInt(10);
                if(number > 5){
                    newValue = 1;
                }
                else{
                    newValue = 0;
                }
                return newValue;
            }
        }
        return newValue;
    }

}